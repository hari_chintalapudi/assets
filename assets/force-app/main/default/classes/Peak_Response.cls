// ===================
// Custom Response wrapper
// ===================
public with sharing class Peak_Response {
    @AuraEnabled public Boolean success{get;set;} // success or failure
    @AuraEnabled public List<String> messages{get;set;}  // messages to display
    @AuraEnabled public List<SObject> results{get;set;} // results to iterate over - use this when querying SOjbects directoy
    @AuraEnabled public List<Peak_ContentObject> peakResults{get;set;} // custom results to iterate over - use this when creating a list of custom wrapper classes
    @AuraEnabled public BHF_OLS_BoBWrapper bookOfBusiness{get;set;}
    @AuraEnabled public User user{get;set;} // success or failure
    @AuraEnabled public salesforce_ols_contract__x contract{get;set;} // contract record
    @AuraEnabled public List<Object> wrapperResults {get; set;}

    public Peak_Response(){
        success = true;
        messages = new List<String>();
        results = new List<SObject>();
        peakResults = new List<Peak_ContentObject>();
        bookOfBusiness = new BHF_OLS_BoBWrapper();
        user = new User();
        contract = new salesforce_ols_contract__x();
        wrapperResults = new List<Object>();
    }

}