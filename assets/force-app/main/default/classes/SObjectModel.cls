public class SObjectModel {
 
    public static List<SObject> get(sObjectType context, String whereby, Set<String> recordId, List<String> extraFields, String orderBy) {
        List<SObject> rs = new List<SObject>();
        if (context == null || whereby == null || recordId == null) return rs;
        DescribeSObjectResult table = context.getDescribe();
        List<String> fieldList = getFieldnames(table);
        if (extraFields != null && !extraFields.isEmpty()) fieldList.addAll(extraFields);
        if(orderBy == null) orderBy = '';
        String query = 'select ' + String.join(fieldList, ',') + ' from ' + table.getName() + ' where ' + whereby + ' = :recordId ' + orderBy;
        system.debug('##########' + query);
        return Database.query(query);
    }
  
    public static SObject single(sObjectType context, String recordId, List<String> extraFields) {
        return single(context, 'Id', recordId, extraFields);
    }
  
    public static SObject single(sObjectType context, String whereBy, String recordId, List<String> extraFields) {
        return single(context, whereBy, recordId, extraFields, null);
    }
    public static SObject single(sObjectType context, String whereBy, String recordId, List<String> extraFields, String orderBy) {
        SObject[] rs = get(context, whereBy, new Set<String>{recordId}, extraFields, orderBy);
        if (rs.isEmpty()) return context.newSObject();
        return rs.get(0);
    }
  
    public static String truncate(String input, Integer to) {
        if(input == null) return '';
        if(input.length() > to) input = input.substring(0, to);
        return input;
    }
  
    static List<String> getFieldnames(DescribeSObjectResult describe) {
        List<String> fieldList = new List<String>();
        fieldList.addAll(describe.fields.getMap().keySet());
        return fieldList;
    }
  
  
 }
 
 