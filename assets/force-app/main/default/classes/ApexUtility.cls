public with sharing class ApexUtility {
    public static String formatPhone(String input) {
        if(input == null) return input;
        String rs = input.replaceAll('[^0-9]', '');
        if(rs.length() != 10){
            if(rs.length() == 11 && rs.substring(0,1) == '1') return rs.substring(1);
        }
        return rs;
    }
    public static String iso8601(Date dt) {
        if (dt == null) return null;
        Datetime rs = Datetime.newInstance(dt, Time.newInstance(0, 0, 0, 0));
        return rs.format('yyyy-MM-dd');
    }
    
    public static String toString(String input) {
        if (input == null) return null;
        return String.ValueOf(input);
    }
  
    public static String join(List<String> input, String chr) {
        if (input == null) return null;
        return String.join(input, chr);
    }
    
    public static String uuid() {
        Blob b = Crypto.GenerateAESKey(128);
        String h = EncodingUtil.ConvertTohex(b);
        String guid = h.SubString(0, 8) + '-' + h.SubString(8, 12) + '-' + h.SubString(12, 16) + '-' + h.SubString(16, 20) + '-' + h.substring(20);
        return guid;
    }

    public static String truncate(String input, Integer lim) {
        if(input == null) return null;
        if(input.length() > lim) {
            return input.substring(0, lim);
        }
        return input;
    }
 
 
 
}
