({
   
    clearPhotos : function(component, event, helper) {
        component.set("v.isClearPhoto", true);
        self.doInit(component, event, helper);
        component.set("v.isClearPhoto", false);
    },
    
    doInit : function(component, event, helper) {
        var buttonName=component.get('v.buttonName') ;
        
        if(component.get("v.isClearPhoto") == false){
            if (confirm('Click Ok For Front Camera')) {
                buttonName = 'frontCamera';
            } else {
                buttonName = 'rearCamera';
            } 
        }
        component.set('v.buttonName',buttonName) ;
        
        var width = 400; // scale the photo width to this
        var height = 0; // computed based on the input stream

     	var streaming = false;
        var video = null;
        var canvas = null;
        var photo = null;
        var startbutton = null;
        var savebutton = null;
    	videoElem = document.getElementById('video');
        video = document.getElementById('video');
        canvas = document.getElementById('canvas');
        photo = document.getElementById('photo');
        startbutton = document.getElementById('startbutton');
        var clearbutton = document.getElementById('clearbutton');
        savebutton = document.getElementById('savebutton');
        
        
        if(buttonName =='rearCamera'){
            navigator.mediaDevices.getUserMedia({ audio: false, video: { facingMode: { exact: "environment" } } })
        .then(function(stream) { 
            console.log("[rearCamera stream]", stream);
            video.srcObject = stream;
            video.play();
        })
        .catch(function(err) {
            console.log("An error occurred: " + err);
        });
        } 
        
        if(buttonName =='frontCamera'){ 
            navigator.mediaDevices.getUserMedia({ audio: false, video: { facingMode: "user" } })
            .then(function(stream) { 
                console.log("[frontCamera stream]", stream);
                video.srcObject = stream;
                video.play();
            })
            .catch(function(err) {
                console.log("An error occurred: " + err);
            });
        }
        video.addEventListener('canplay', function(ev){
            if (!streaming) {
                height = video.videoHeight / (video.videoWidth/width);
                // Firefox currently has a bug where the height can't be read from
                // the video, so make assumptions if this happens.
                if (isNaN(height)) {
                    height = width / (4/3);
                }
                
                video.setAttribute('width', width);
                video.setAttribute('height', height);
                canvas.setAttribute('width', width);
                canvas.setAttribute('height', height);
                streaming = true;
            }
        }, false);
        
        startbutton.addEventListener('click', function(ev){
            takepicture();
        }, false);
        
        clearbutton.addEventListener('click', function(ev){
            clearphoto();
        }, false);
        
        
        clearphoto();
        function clearphoto() {
            console.log('Inside clearphoto');
            var context = canvas.getContext('2d');
            context.fillStyle = "#AAA";
            context.fillRect(0, 0, canvas.width, canvas.height);
            var data = canvas.toDataURL('image/png');
            photo.setAttribute('src', data);
        }
        
      	function takepicture() {
            var context = canvas.getContext('2d');
            if (width && height) {
                canvas.width = width;
                canvas.height = height;
                context.drawImage(video, 0, 0, width, height);
                
                var data = canvas.toDataURL('image/png');
                photo.setAttribute('src', data);
                //console.log("+++++"+data);
				data = data.replace('data:image/png;base64,', ""); 
                component.set("v.data",data);
            } else {
                clearphoto();
            }
            track.stop();
            component.set("v.stopVideo", false); 
        }
    },
    
    savebutton : function(component, event, helper) {
        var data = component.get("v.data");
        //console.log("after replce+++++"+data);
        //data = Encodingutil.base64Decode(data);
        helper.uploadHelper(component, event,data);
        //helper.stopVideo(component, event, helper);
    },

})