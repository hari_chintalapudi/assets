/*
 * Source : sfdcmonkey.com 
 * Date : 25/9/2017
 * Locker Service Ready code.
 */
({
    MAX_FILE_SIZE: 4500000, //Max file size 4.5 MB 
    CHUNK_SIZE: 750000,      //Chunk Max size 750Kb 
    
    uploadHelper: function(component, event,data) {
        console.log('in upload helper start');
        
        var file =data;
        var self = this;
        var fileContents = data;
        console.log('in upload helper end'+fileContents.length);
        self.uploadProcess(component, file, fileContents);
        objFileReader.readAsDataURL(file);
    },
    
    uploadProcess: function(component, file, fileContents) {
        console.log('in uploadProcess start');
        // set a default size or startpostiton as 0 
        var startPosition = 0;
        // calculate the end size or endPostion using Math.min() function which is return the min. value   
        var endPosition = Math.min(fileContents.length, startPosition + this.CHUNK_SIZE);
        
        // start with the initial chunk, and set the attachId(last parameter)is null in begin
        this.uploadInChunk(component, file, fileContents, startPosition, endPosition, '');
        console.log('in uploadProcess end');
    },
    
    
    uploadInChunk: function(component, file, fileContents, startPosition, endPosition, attachId) {
        console.log('in uploadInChunk start');
        // call the apex method 'saveChunk'
        component.set("v.showLoadingSpinner", true);
        var getchunk = fileContents.substring(startPosition, endPosition);
        var action = component.get("c.saveChunk");
        /*action.setParams({
            parentId: component.get("v.parentId"),
            fileName: file.name,
            base64Data: encodeURIComponent(getchunk),
            contentType: file.type,
            fileId: attachId
        });*/
        
        action.setParams({
            parentId: component.get("v.uploadltroDocumentId"),
            fileName: 'text',
            base64Data: encodeURIComponent(getchunk),
            contentType: '.jpeg',
            fileId: attachId
        });
        
        // set call back 
        action.setCallback(this, function(response) {
            // store the response / Attachment Id   
            attachId = response.getReturnValue();
            var state = response.getState();
            if (state === "SUCCESS") {
                // update the start position with end postion
                startPosition = endPosition;
                endPosition = Math.min(fileContents.length, startPosition + this.CHUNK_SIZE);
                // check if the start postion is still less then end postion 
                // then call again 'uploadInChunk' method , 
                // else, diaply alert msg and hide the loading spinner
                if (startPosition < endPosition) {
                    this.uploadInChunk(component, file, fileContents, startPosition, endPosition, attachId);
                } else {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "type":"Success",
                        "message": "The File has been uploaded successfully."
                    });
                    toastEvent.fire();
                    var vx = component.get("v.method");
                    //fire event from child and capture in parent
                    $A.enqueueAction(vx);
                    //helper.showMessage("Success","Record uploaded Successfully","Success");
                    //alert('your File is uploaded successfully');
                    component.set("v.showLoadingSpinner", false);
                }
                // handel the response errors        
            } else if (state === "INCOMPLETE") {
                alert("From server: " + response.getReturnValue());
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        console.log('in uploadInChunk end');
        // enqueue the action
        $A.enqueueAction(action);
    },
    
    stopVideo : function(component, event, helper) {
        var videoElem = document.getElementById('video');  
        console.log('videoElem :',videoElem);
        var stream = videoElem.srcObject;
        console.log('stream :',stream);
        var tracks = stream.getTracks();
        console.log('tracks :',tracks);
        tracks.forEach(function(track) {
            track.stop();
        });
        videoElem.srcObject = null;
        //stream.stop();
    }
})