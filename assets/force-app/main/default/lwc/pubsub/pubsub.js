/**
 * A basic pub-sub mechanism for sibling component communication
 *
 * TODO - adopt standard flexipage sibling communication mechanism when it's available.
 */
import {ShowToastEvent} from 'lightning/platformShowToastEvent';
const events = {};

/**
 * Confirm that two page references have the same attributes
 * @param {object} pageRef1 - The first page reference
 * @param {object} pageRef2 - The second page reference
 */
const samePageRef = (pageRef1, pageRef2) => {
  const obj1 = pageRef1.attributes;
  const obj2 = pageRef2.attributes;
  return Object.keys(obj1)
    .concat(Object.keys(obj2))
    .every((key) => {
      return obj1[key] === obj2[key];
    });
};

/**
 * Registers a callback for an event
 * @param {string} eventName - Name of the event to listen for.
 * @param {function} callback - Function to invoke when said event is fired.
 * @param {object} thisArg - The value to be passed as the this parameter to the callback function is bound.
 */
const registerListener = (eventName, callback, thisArg) => {
  // Checking that the listener has a pageRef property. We rely on that property for filtering purpose in fireEvent()
  if (!thisArg.pageRef) {
    throw new Error(
      'pubsub listeners need a "@wire(CurrentPageReference) pageRef" property'
    );
  }

  if (!events[eventName]) {
    events[eventName] = [];
  }
  const duplicate = events[eventName].find((listener) => {
    return listener.callback === callback && listener.thisArg === thisArg;
  });
  if (!duplicate) {
    events[eventName].push({
      callback,
      thisArg
    });
  }
};

/**
 * Unregisters a callback for an event
 * @param {string} eventName - Name of the event to unregister from.
 * @param {function} callback - Function to unregister.
 * @param {object} thisArg - The value to be passed as the this parameter to the callback function is bound.
 */
const unregisterListener = (eventName, callback, thisArg) => {
  if (events[eventName]) {
    events[eventName] = events[eventName].filter(
      (listener) =>
      listener.callback !== callback || listener.thisArg !== thisArg
    );
  }
};

/**
 * Unregisters all event listeners bound to an object.
 * @param {object} thisArg - All the callbacks bound to this object will be removed.
 */
const unregisterAllListeners = (thisArg) => {
  Object.keys(events).forEach((eventName) => {
    events[eventName] = events[eventName].filter(
      (listener) => listener.thisArg !== thisArg
    );
  });
};

/**
 * Fires an event to listeners.
 * @param {object} pageRef - Reference of the page that represents the event scope.
 * @param {string} eventName - Name of the event to fire.
 * @param {*} payload - Payload of the event to fire.
 */
const fireEvent = (pageRef, eventName, payload) => {
  if (events[eventName]) {
    const listeners = events[eventName];
    listeners.forEach((listener) => {
      if (samePageRef(pageRef, listener.thisArg.pageRef)) {
        try {
          listener.callback.call(listener.thisArg, payload);
        } catch (error) {
          // fail silently
        }
      }
    });
  }
};
/**
 * Calculates age on the basis of entered date.
 * @param {date} birthdate - Birth date of the contact.
 */
const  calculateAge = (date) =>{
  console.log('inside age:');
  // if (!this.validateDate(date)) {
  //     return;
  // }
  var Bday = +new Date(date);
  let age = ~~((Date.now() - Bday) / (31557600000));
  return age;
};
const validateEmail = (email) => {
 //var regExpEmail = /^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*(\+[a-zA-Z0-9-]+)?@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*$/;
//let regExpEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
//let regExpEmail = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
let regExpEmail = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
//let regExpEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  var isValid = email.match(regExpEmail);
  console.log("isValid", isValid);
  return isValid;
};
const validateDate = (date) => {
  var regExpPhone = /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/;
  var isValid = date.match(regExpPhone);
  console.log("isValid", isValid);
  return isValid;
};
const reduceErrors = (errors) => {
  if (!Array.isArray(errors)) {
      errors = [errors];
  }

  return (
      errors
          // Remove null/undefined items
          .filter(error => !!error)
          // Extract an error message
          .map(error => {
              // UI API read errors
              if (Array.isArray(error.body)) {
                  return error.body.map(e => e.message);
              }
              // UI API DML, Apex and network errors
              else if (error.body && typeof error.body.message === 'string') {
                  return error.body.message;
              }
              // JS errors
              else if (typeof error.message === 'string') {
                  return error.message;
              }
              // Unknown error shape so try HTTP status text
              return error.statusText;
          })
          // Flatten
          .reduce((prev, curr) => prev.concat(curr), [])
          // Remove empty strings
          .filter(message => !!message)
  );
        };
  const validateUserInput = (_this) =>{
    const allValid = [..._this.template.querySelectorAll('.inputField')]
    .reduce((validSoFar, inputCmp) => 
    {
      inputCmp.reportValidity();
      return validSoFar && inputCmp.checkValidity();
    }, true);

    if (allValid) {
      console.log('All form entries look valid. Ready to submit!');
    } else {
      console.log('Please update the invalid form entries and try again.');
    }
    console.log('here is the valid user input method',allValid);
    return allValid;
};
const showToast = (_this,theTitle, theMessage, theVariant) => {
  console.log('showToast called');
  const event = new ShowToastEvent({
    title: theTitle,
    message: theMessage,
    variant: theVariant
  });
  _this.dispatchEvent(event);
  console.log('showToast event fired');
};

const getTodayDate = () =>{
  let today = new Date(),
    day = today.getDate(),
    month = today.getMonth() + 1, //January is 0
    year = today.getFullYear();
  if (day < 10) {
    day = '0' + day;
  }
  if (month < 10) {
    month = '0' + month;
  }
  today = year + '-' + month + '-' + day;
   console.log('get today: ', today);
  return today;
};
const validNames =(name) =>{
  let numbers = /^([^0-9]*)$/;
  let inputtxt = name;
  if (numbers.test(inputtxt) && inputtxt != '' && typeof inputtxt != 'undefined' && inputtxt.trim().length >0) {  
    return true;
  } else {
    return false;
  }
};

function formatPhoneNumber(keyCode, number) {
  //let keyCode = event.which;
  console.log(typeof keyCode, keyCode < 48, keyCode > 57);
  if ((keyCode < 48 || keyCode > 57)) {
      preventDefault();
      console.log('inside the check',number);
      return number;
  } 
  return this.autoFormatNumber(number);
};
function autoFormatNumber(number) {
   console.count();
  number = number.replace(/[^\d]/g, '');
  if (number.length === 1) {
      number = number.replace(/(\d{1})/, "($1)");
  } else if (number.length === 2) {
      number = number.replace(/(\d{2})/, "($1)");
  } else if (number.length === 3) {
      number = number.replace(/(\d{3})/, "($1)");
  } else if (number.length === 4) {
      number = number.replace(/(\d{3})(\d{1})/, "($1) $2");
  } else if (number.length === 5) {
      number = number.replace(/(\d{3})(\d{2})/, "($1) $2");
  } else if (number.length === 6) {
      number = number.replace(/(\d{3})(\d{3})/, "($1) $2");
  } else if (number.length === 7) {
      number = number.replace(/(\d{3})(\d{3})(\d{1})/, "($1) $2-$3");
  } else if (number.length === 8) {
      number = number.replace(/(\d{3})(\d{3})(\d{2})/, "($1) $2-$3");
  } else if (number.length === 9) {
      number = number.replace(/(\d{3})(\d{3})(\d{3})/, "($1) $2-$3");
  } else if (number.length === 10) {
      number = number.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3");
  }
  return number;
}
  

export {
  registerListener,
  unregisterListener,
  unregisterAllListeners,
  fireEvent,
  calculateAge,
  validateEmail,
  validateDate,
  reduceErrors,
  validateUserInput,
  showToast,
  getTodayDate,
  validNames,
  formatPhoneNumber,
  autoFormatNumber
};